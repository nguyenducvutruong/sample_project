﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ImageViewer : MonoBehaviour
{
    [SerializeField] MeasureDepth m_MeasureDepth;
    [SerializeField] MultiSourceManager m_MultiSource;

    [SerializeField] RawImage m_RawImage;
    [SerializeField] RawImage m_RawDepth;

    // Update is called once per frame
    void Update()
    {
        m_RawImage.texture = m_MultiSource.GetColorTexture();
        m_RawDepth.texture = m_MeasureDepth.m_DepthTexture;
    }
}
