﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bubble : MonoBehaviour
{
    private Vector3 m_MovingDirection = Vector3.zero;
    private Coroutine m_CurrentChanger = null;
    // Start is called before the first frame update
    void Start()
    {

    }

    private void OnEnable()
    {
        m_CurrentChanger = StartCoroutine(DirectionChanger());
    }
    private void OnDisable()
    {
        StopCoroutine(m_CurrentChanger);
    }
    private void OnBecameInvisible()
    {
        this.gameObject.SetActive(false);
    }
    // Update is called once per frame
    void Update()
    {
        this.transform.position += m_MovingDirection * 0.5f * Time.deltaTime;


    }
    private IEnumerator DirectionChanger()
    {
        while (this.gameObject.activeSelf)
        {
            m_MovingDirection = new Vector2(Random.Range(0, 100) * 0.01f, Random.Range(0, 100) * 0.01f);
            yield return new WaitForSeconds(3f);
        }
    }
}
