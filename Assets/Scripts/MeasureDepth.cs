﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Windows.Kinect;

public class MeasureDepth : MonoBehaviour
{
    public Texture2D m_DepthTexture;
    [SerializeField] MultiSourceManager m_MultiSource;

    //Cutoffs
    public float m_Sensitivity = 1;
    public float m_WallDepth = -10f;
    [Range(-1f,1f)]
    public float m_TopCutoff = 1f;
    [Range(-1f, 1f)]
    public float m_BottomCutoff = -1f;
    [Range(-1f, 1f)]
    public float m_LeftCutoff = -1f;
    [Range(-1f, 1f)]
    public float m_RightCutogg = 1f;


    private ushort[] m_DepthData = null;
    private CameraSpacePoint[] m_CameraSpacePoints = null;
    private ColorSpacePoint[] m_ColorSpacePoints = null;
    private List<ValidPoint> m_ValidPoints = null;

    private KinectSensor m_KinectSensor = null;
    private CoordinateMapper m_Mapper = null;
    private Camera m_Camera;

    private readonly Vector2Int m_DepthResolution = new Vector2Int(512, 424);

    private Rect m_Rect;


    public float time = 3f;
    private float counter = 3f;

    private void Awake()
    {
        m_Camera = Camera.main;
        m_KinectSensor = KinectSensor.GetDefault();
        m_Mapper = m_KinectSensor.CoordinateMapper;

        int ArraySize = m_DepthResolution.x * m_DepthResolution.y;

        m_CameraSpacePoints = new CameraSpacePoint[ArraySize];
        m_ColorSpacePoints = new ColorSpacePoint[ArraySize];
    }
    private void Update()
    {
        //if (Input.GetKeyDown(KeyCode.Space))
        counter -= Time.deltaTime;
        if(counter <= 0f)
        {
            m_ValidPoints = DepthToColor();
            m_Rect = CreateRect(m_ValidPoints);
            m_DepthTexture = CreateTexture(m_ValidPoints);

            counter = time;
        }
    }
    private void OnGUI()
    {
        GUI.Box(m_Rect, "");
    }
    private List<ValidPoint> DepthToColor()
    {
        List<ValidPoint> validPoints = new List<ValidPoint>();
        m_DepthData = m_MultiSource.GetDepthData();

        //Map ???
        m_Mapper.MapDepthFrameToCameraSpace(m_DepthData, m_CameraSpacePoints);
        m_Mapper.MapDepthFrameToColorSpace(m_DepthData, m_ColorSpacePoints);

        //filter
        for(int i = 0; i< m_DepthResolution.x / 8; i++)
        {
            for(int j = 0; j < m_DepthResolution.y / 8; j++)
            {
                int sampleIndex = (j * m_DepthResolution.x) + i;
                sampleIndex *= 8;

                if (m_CameraSpacePoints[sampleIndex].X < m_LeftCutoff)
                    continue;
                if (m_CameraSpacePoints[sampleIndex].X > m_RightCutogg)
                    continue;
                if (m_CameraSpacePoints[sampleIndex].Y > m_TopCutoff)
                    continue;
                if (m_CameraSpacePoints[sampleIndex].Y < m_BottomCutoff)
                    continue;

                ValidPoint newPoint = new ValidPoint(m_ColorSpacePoints[sampleIndex], m_CameraSpacePoints[sampleIndex].Z);
                if(m_CameraSpacePoints[sampleIndex].Z >= m_WallDepth)
                {
                    newPoint.m_WithinWallDepth = true;
                }

                validPoints.Add(newPoint);
            }
        }

        return validPoints;
    }
    private Texture2D CreateTexture(List<ValidPoint> validPoints)
    {
        Texture2D newTexture = new Texture2D(1920, 1080, TextureFormat.Alpha8, false);
        for(int x = 0; x < newTexture.width;x++)
        {
            for(int y = 0; y < newTexture.height; y++)
            {
                newTexture.SetPixel(x, y, Color.clear);
            }
        }
        foreach(ValidPoint validPoint in validPoints)
        {
            newTexture.SetPixel((int)validPoint.colorSpace.X, (int)validPoint.colorSpace.Y, Color.black);
        }

        newTexture.Apply();

        return newTexture;
    }

    private Rect CreateRect(List<ValidPoint> validPoints)
    {
        if(validPoints.Count == 0)
        {
            return new Rect();
        }
        Vector2 topLeft = GetTopLeft(validPoints);
        Vector2 bottomRight = GetBottomRight(validPoints);

        //Translate to viewport
        Vector2 screenTopLeft = ScreenToCamera(topLeft);
        Vector2 screenBottomRight = ScreenToCamera(bottomRight);

        //Rect Demension
        int width = (int)(screenBottomRight.x - screenTopLeft.x);
        int height = (int)(screenBottomRight.y - screenTopLeft.y);

        //Create
        Vector2 size = new Vector2(width, height);
        Rect rect = new Rect(screenTopLeft, size);

        return rect;
    }

    private Vector2 GetTopLeft(List<ValidPoint> validPoints)
    {
        Vector2 result = new Vector2(int.MaxValue, int.MaxValue);

        foreach(ValidPoint point in validPoints)
        {
            if(point.colorSpace.X <= result.x)
            {
                result.x = point.colorSpace.X;
            }

            if(point.colorSpace.Y <= result.y)
            {
                result.y = point.colorSpace.Y;
            }
        }

        return result;
    }

    private Vector2 GetBottomRight(List<ValidPoint> validPoints)
    {
        Vector2 result = new Vector2(0, 0);

        foreach (ValidPoint point in validPoints)
        {
            if (point.colorSpace.X >= result.x)
            {
                result.x = point.colorSpace.X;
            }

            if (point.colorSpace.Y >= result.y)
            {
                result.y = point.colorSpace.Y;
            }
        }

        return result;
    }

    private Vector2 ScreenToCamera(Vector2 screenPosition)
    {
        Vector2 normalizedScreen = new Vector2(Mathf.InverseLerp(0, 1920, screenPosition.x), Mathf.InverseLerp(0, 1080, screenPosition.y));
        Vector2 screenPoint = new Vector2(normalizedScreen.x * m_Camera.pixelWidth, normalizedScreen.y * m_Camera.pixelHeight);

        return screenPoint;
    }
}

public class ValidPoint
{
    public ColorSpacePoint colorSpace;
    public float z = 0.0f;

    public bool m_WithinWallDepth = false;
    public ValidPoint(ColorSpacePoint newColorSpacePoint, float newZ)
    {
        this.colorSpace = newColorSpacePoint;
        this.z = newZ;
    }
}
