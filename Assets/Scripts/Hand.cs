﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hand : MonoBehaviour
{
    [SerializeField] Transform m_HandMesh;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        m_HandMesh.position = Vector3.Lerp(m_HandMesh.position, this.transform.position, Time.deltaTime * 15f);
    }
    private void OnTriggerEnter2D(Collider2D collider)
    {
        if(collider.gameObject.tag == "Bubble")
        {
            collider.gameObject.SetActive(false);
        }
    }
}
